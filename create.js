#!/usr/bin/env node

var filename = 'index.md';
var markdownpdf = require('markdown-pdf'), fs = require('fs');
fs.createReadStream(filename)
    .pipe(markdownpdf())
    .pipe(fs.createWriteStream('./resume.pdf'))
