---
layout: default
permalink: /resume/
---

# 🔎 Barry Teoh Résumé

## 📋 Professional Summary

* I can write [code](https://github.com/nolim1t) (Front and Backend, but have a preference for backend) and manage backend infrastructure (using AWS / Docker)
* Do-er (I've attended startupweekends, hackathons, and co-founded startups with exits in the past)
* I ❤︎ UX (My passion is to make software great again)

## 📫 Contact Information

* <i class="fa fa-telegram" aria-hidden="true"></i> [Telegram](https://t.me/nolim1t)
* <i class="fa fa-commenting-o" aria-hidden="true"></i> [Messenger](https://m.me/barry.teoh)

## 🧰 Technology Stack / Skills ("My Toolbox")

* Fluent Javascript (Node / React / Vue / Angular / NPM Authoring)
* Fluent Ruby (Rails / RubyGem Authoring)
* Fluent Python 3.0
* Fluent Swift / Objective-C
* Fluent in HTML / CSS
* Fluent in System administration (AWS / Docker)
* PaaS (AWS Lambda, Parse / Parse Migrations, Heroku)
* Devops and CI/CD (Docker, buildx, Gitlab CI, Github Actions), including deployment to IoT devices
* Blockchain Technology (JSON-RPC interfacing, and Solidity)
* Administrative (Client facing, project management, writing tech documentation, some language translation, Issue tracking, CRM systems, etc)

## 💼 Portfolio ("Proof of Work")

* [My Open Source](http://nolim1t.co/opensource)
* [My Projects](https://nolim1t.co/projects)

## Noteable clients worked with in the past

* Saatchi & Saatchi
* ING Direct
* Multiple Australian State & Federal Government agencies

## Experience
### Highlights / Recent / Ongoing
#### Super Secret Shadow Coder | Earth 🌏
* Full Stack Developer (UX / VUE.js / React / Swift / Objective-C / Node / Golang / DevOps)
* Docker container automation
* Reproduceable builds using gitlab and github CI systems
* Blockchain and general technology consultant for various startups (2017 - ongoing)

#### 💎🙌💼 Diamond Hands Limited. / บริษัทมือเพชรจำกัด - Principal consultant & fund manager 
* Monitoring finances and providing reports for all stakeholders
* Providing support for all online payment systems
* Facilitating events for improving the ecosystem

### Other Roles
* Ispolink - Blockchain Developer
* The Unicorn Factory 獨角獸工廠 - Principle Consultant 
* GetUmbrel.com - Initial Founder & Current OG Contributor
* LNCM.io - Founding team & Current OG Contributor
* BobbyBNB / Bobbybean Design (Taiwan) - Founder
* Kuggle Limited (Taiwan) - Founder
* Web Summit 2017 Conference (Portugal) - Startup Team Volunteer
* RISE 2017 Conference (Hong Kong) - Operations Team Volunteer
* Sydney Gay & Lesbian Mardi Gras (Australia) - Entrants Team Volunteer (2015 / 2017)
* Startup Weekend Hong Kong - Volunteer Event Organizer
* Viocorp International (Australia) - Quality Assurance
* The Dubs Agency (Australia) - Mobile App Developer
* Mobile Messenger / Verisign (USA) - Quality Assurance / Infrastructure
* Catjes / MLA (Australia) - Quality Assurance
* Networx Hosting (USA)- Founder
* World.Net Services (Australia / Malaysia) - Support / Quality Assurance
