# Resume Project

## About

My online resume, with my tech blog styling

## How

* This is my online resume with my tech blog styling. Used as a submodule for my tech blog.

In the jekyll website.

```bash
git submodule add https://gitlab.com/nolim1t/resume.git
git submodule foreach git pull origin master
```
